using System;
using Xunit;

namespace Chaine_de_caractère_test
{
    public class UnitTest1
    {
        [TestMethode]
        static void TestMajuscule()
        {
            Assert.AreEqual(true, Program.CommenceMajuscule("Entrer une phrase"));
            Assert.AreEqual(false, Program.CommenceMajuscule("entrer une phrase"));
        }
        [TestMethode]
        static void TestPoint()
        {
            Assert.AreEqual(true, Program.FinitPoint("Entrer une phrase,"));
            Assert.AreEqual(false, Program.FinitPoint("Entrer une phrase."));
        }
        [TestMethode]
        static void TestEntree()
        {
            Assert.AreEqual(0, Program.ControleSaisie("Entrer une phrase"));
            Assert.AreEqual(0, Program.ControleSaisie("18 Entrer une phrase"));
            Assert.AreEqual(18, Program.ControleSaisie("18"));
        }
    }
}
